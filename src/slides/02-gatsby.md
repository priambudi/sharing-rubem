![gatsby](./gatsby_logo.svg)

---

## Static Site !== Static Page
Masih bisa bikin situs menjadi dinamis.

---

## 💪 GraphQL powered

* One of its most powerful features is its ability to get only the data you ask for— **you decide** what you want and need, not the server/source.
* Therefore, Gatsby doesn't need a classic backend and GraphQL lets you query all necessary data from wherever you want: markdown files, databases, Storyblok, traditional CMSs like Drupal and so on.

___

## 🔌 Plugin architecture
Memungkinkan untuk menambahkan fitur tambahan, ***plugin***, ke ***core*** aplikasi.
* ✅ ***extendable***
* ✅ ***flexible***
* ✅ ***isolation of application features***

---

## ⚛️ React-based
Situs Gatsby mudah diimplementasi kalau sudah kenal react.

---

## Kenapa Gatsby? 🤔

---

## 🚀 Dibuat dengan mengutamakan performa
* Optimisasi gambar.
* Konfigurasi bundler (*webpack*) paling optimal.
* Prefetch resource, jadi mondar-mandir di situs jadi lebih cepat

---

## Berbasis teknologi populer dan berpengaruh
* **React**. Mudah dipelajari, kode dapat digunakan ulang di mana saja.
* **GraphQL**.

---

## 🔑 Keamanan
Sangat aman karena hanya mengirim HTML.

---

## Situs resmi menarik
![gatsby-site](./gatsby-web.png)

___

## Dokumentasi/tutorial banyak dan mudah diikuti
![gatsby-tutorial](./gatsby-tutorial.png)

___

## Gatsby Downloads
![chart1](./data-gatsby.png)

---

## Gatsby & Next Downloads
![chart2](./data-gatsby-next.png)

---

## Gatsby & Next & React Downloads
![chart4](./data-gatsby-next-react.png)