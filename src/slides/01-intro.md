# Create blazing fast websites

with Gatsby JS

---

> 1. Client-Side Rendering vs. Server-Side Rendering
> 2. Static Site vs. Dynamic Site
> 3. Gatsby (Static Site Generator)

---

> Learning to code is useful
> no matter what your career ambitions are.
> <cite>Arianna Huffington, Founder, The Huffington Post</cite>

---

## Client-Side Rendering
Me-*render* konten HTML pada browser klien dengan menggunakan Javascript.
* ReactJS.

---

## Client-Side Rendering
![csr](./csr.png)

(source: [yudhajitadhikary](https://yudhajitadhikary.medium.com/client-side-rendering-vs-server-side-rendering-in-react-js-next-js-b74b909c7c51))

---

## Client-Side Rendering
* (+) Lebih mudah dibuat dan dikembangkan.
* (+) Situs akan berkesan ***seamless***.

---

## Client-Side Rendering
* (-) Klien harus ada Javascript. 🤡
* (-) Kurang baik untuk SEO. 🤡
* (-) Awal buka halaman butuh waktu lama. 🤡
  
---

## Server-Side Rendering
Ada manipulasi (*render*) file HTML pada server sebelum diberikan kepada client.
* Django.

---

## Server-Side Rendering
![ssr](./ssr.png)

(source: [yudhajitadhikary](https://yudhajitadhikary.medium.com/client-side-rendering-vs-server-side-rendering-in-react-js-next-js-b74b909c7c51))

---

## Server-Side Rendering
* (+) Dapat meningkatkan SEO.
* (+) Awal buka halaman akan lebih cepat.
* (+) Cocok untuk *Static Site*.

---

## Server-Side Rendering
* (-) Request ke server makin banyak. 🤡
* (-) Membuka halaman baru perlu reload 1 halaman. 🤡

---

## 🏎️ Bisa lebih cepat? 🏎️

---

## Hybrid
* Utamanya adalah CSR.
* Inject data penting ke dalam HTML sebelum dikirim ke klien.
* (-) ***Initial Page Load*** masih lama. 🤡

---

## 🏎️💨 Bisa lebih cepat lagi? 🏎️💨

---

## Static Site 🌟
Murni HTML dan CSS. Tidak ada database dan ajax.

---

## Static Site vs. Dynamic Site 
![static-site-vs-dynamic-site](./compare.png)

(source: [scotch.io/bar-talk/5-reasons-static-sites-rock](https://scotch.io/bar-talk/5-reasons-static-sites-rock))

---

![compare](./static-site-generator.jpg)

Membuat file *static* HTML yang akan dideploy ke server.
* Tidak ada query data di dalam server.
* Semua query data dilakukan saat ***buildtime***.

---

## Static Site Generators
![static-site-generators](./devopedia-static-site-generators.jpg)

(source: [devopedia](https://devopedia.org/static-site-generators))

